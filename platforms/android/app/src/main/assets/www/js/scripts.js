function SaveItem() {
	var id =   document.forms.AgendaList.id.value;
	var name = document.forms.AgendaList.name.value;
	var data = document.forms.AgendaList.data.value;
	var important;
	try { important = document.querySelector('.form-check-input:checked').value; } catch { important = "off"; }

	var datalist = { 'id': id, 'name': name, 'data': data, 'important' : important  };

	if (name && data) {
		localStorage.setItem(id, JSON.stringify( datalist) );
		$("#datepicker").val("");
		$("#description").val("");
		$('.form-check-input').prop('checked', false);


		cordova.plugins.notification.local.schedule({
		    title: 'Recordatorio Check days',
		    text: name,
		    trigger: { at: new Date(2018, 11, 23, 19) }
		});

		doShowAll();
	}
}

function RemoveItem(value) {
	document.forms.AgendaList.id.value= localStorage.removeItem(value);
	doShowAll();
}

function doShowAll() {

	if (CheckBrowser()) {
		var key = "";
		var list = "<ul>";
		var i = 0;
		var dataitems=[];
		var max_id=0;
		for (i = 0; i <= localStorage.length - 1; i++) {
			key = localStorage.key(i);
			var datalist = JSON.parse( localStorage.getItem(key) );			
			dataitems[i] = datalist;
			if (dataitems[i]['id']>max_id) {
				max_id=dataitems[i]['id'];
			}
		}

		$("#id_store").val(max_id+1);

		dataitems.sort(function(a, b) {
		    var aa = a.data.split('/').reverse().join(),
		        bb = b.data.split('/').reverse().join();
		    return aa < bb ? -1 : (aa > bb ? 1 : 0);
		});
		
		for (i = 0; i < dataitems.length; i++) {
			if (dataitems[i]['id']) {
				list += "<tr class='"+dataitems[i]['important']+"'><td></<td>" + dataitems[i]['name'] + "</td><td>" + dataitems[i]['data'] + "</td><td><input class='btn btn-success' type=button value='¡OK!' onclick='RemoveItem(" + dataitems[i]['id'] + ")' ></td></tr>";
			}
		}		
		list += "</ul>";
		document.getElementById('list').innerHTML = list;
	}
}

function CheckBrowser() {
	if ('localStorage' in window && window['localStorage'] !== null) { return true; } else { return false; }
}
 

$( document ).ready(function() {
	$.datepicker.regional['es'] = {
	   closeText: 'Cerrar',
	   prevText: '< Ant',
	   nextText: 'Sig >',
	   currentText: 'Hoy',
	   monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	   monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	   dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	   dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	   dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	   weekHeader: 'Sm',
	   dateFormat: 'dd/mm/yy',
	   firstDay: 1,
	   isRTL: false,
	   showMonthAfterYear: false,
	   yearSuffix: ''
	 };	
   $.datepicker.setDefaults($.datepicker.regional['es']);
   $("#datepicker").datepicker();	
   //document.addEventListener("deviceready", onDeviceReady, false);
});

function onDeviceReady() {
	cordova.plugins.notification.local.schedule({
	    title: 'Solo una vez',
	    text: 'La primera vez que se ejecuta la APP',
	    foreground: true
	});

	cordova.plugins.notification.local.schedule({
	    title: 'Cada minuto...',
	    trigger: { every: 'minute', count: 10 }
	});

	cordova.plugins.notification.local.schedule({
	    title: 'Cada hora...',
	    trigger: { every: 'hour', count: 10 }
	});

	cordova.plugins.notification.local.schedule({
		id: 7,
	    title: 'Son las 19:00!!',
	    text: 'Tester',
	    trigger: { at: new Date(2018, 11, 23, 19) }
	});




}